import { CustomValidateIsPassword } from '@app/core/pipes/validation.pipe';
import { IsEmail, IsNotEmpty, IsString, MaxLength, MinLength, Validate } from 'class-validator';

export class ClientRegisterDto {
  /**
   * your name
   * @example 0Azhihahaxxxx
   */

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(30)
  name: string;

  /**
   * Email login
   * @example hien.tran@amela.vn
   */
  @IsNotEmpty()
  @MinLength(3)
  @MaxLength(30)
  @IsEmail()
  email: string;

  /**
   * your password login
   * @example 0Azhihahaxxxx
   */

  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @MaxLength(30)
  @Validate(CustomValidateIsPassword)
  password: string;
}
