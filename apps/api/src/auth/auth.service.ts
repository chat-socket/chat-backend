import { Exception } from '@app/core/exception';
import { User } from '@app/database-mongodb/schemas/user';
import { JwtAuthenticationService } from '@app/jwt-authentication';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';
import { IConfigAuth } from 'libs/constants/configuration-env';
import { ErrorCustom, UserType } from 'libs/constants/enum';
import { IPayloadToken, IToken } from 'libs/constants/interface';
import { Model } from 'mongoose';
import { ClientLoginDto } from './dto/login.dto';
import { ClientRegisterDto } from './dto/register.dto';
@Injectable()
export class AuthService {
  constructor(
    private readonly JwtAuthenticationService: JwtAuthenticationService,
    @InjectModel(User.name) private userModel: Model<User>,
    private readonly configService: ConfigService,
  ) {}
  async login({ email, password }: ClientLoginDto) {
    const user = await this.userModel.findOne({ email }).select(['email', 'password', 'refreshToken']);

    if (!user) {
      throw new Exception(ErrorCustom.Email_Not_Found, 'Không tìm thấy email người dùng');
    }

    const isCorrectPassword = await bcrypt.compare(password, user.password);
    if (!isCorrectPassword) {
      throw new Exception(ErrorCustom.Email_Or_Password_Not_valid, 'Email hoặc mật khẩu không chính xác');
    }

    return this.generateToken(user);
  }

  async register({ name, email, password }: ClientRegisterDto) {
    const configAuth = this.configService.get<IConfigAuth>('auth');
    const user = await this.userModel.findOne({ email });

    if (user) {
      throw new Exception(ErrorCustom.Email_User_Already_Account, 'Tài khoản đã đăng kí');
    }

    const hashPassword = await bcrypt.hash(password, configAuth?.saltRound);

    const newUserModel = new this.userModel({
      name,
      email,
      password: hashPassword,
    });

    const newUser = await newUserModel.save();

    return this.generateToken(newUser);
  }

  async generateToken(user, params?: User): Promise<IToken> {
    const data = { _id: user._id, name: user.name, email: user.email };
    const payload: IPayloadToken = { _id: user._id, userType: UserType.CLIENT, timeStamp: new Date().getTime() };
    const token = this.JwtAuthenticationService.generateAccessToken(payload);

    const isValid = this.JwtAuthenticationService.verifyRefreshToken(user?.refreshToken);
    if (!isValid) {
      const newRefreshToken = this.JwtAuthenticationService.generateRefreshToken(payload);

      await this.userModel.updateOne(
        { _id: user._id },
        {
          refreshToken: newRefreshToken,
          ...params,
        },
      );

      return { token, refreshToken: newRefreshToken, ...data };
    }
    return { token, refreshToken: user?.refreshToken, ...data };
  }
}
