import { ClientControllers } from '@app/core/decorator/controller-customer.decorator';
import { Public } from '@app/jwt-authentication/public-api.decorator';
import { Body, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ClientRegisterDto } from './dto/register.dto';
import { LoginResponseSchema } from './auth.schema';
import { ApiResponse } from '@nestjs/swagger';
import { ClientLoginDto } from './dto/login.dto';

@ClientControllers('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  @Public()
  @ApiResponse(LoginResponseSchema)
  login(@Body() body: ClientLoginDto) {
    return this.authService.login(body);
  }

  @Post('register')
  @Public()
  @ApiResponse(LoginResponseSchema)
  register(@Body() body: ClientRegisterDto) {
    return this.authService.register(body);
  }
}
