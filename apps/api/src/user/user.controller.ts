import { ClientControllers } from '@app/core/decorator/controller-customer.decorator';
import { Public } from '@app/jwt-authentication/public-api.decorator';
import { Get, Param } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { UserService } from './user.service';
import { ValidateMongoIdPipe } from '@app/core/pipes/validation.pipe';
import { DetailUserResponseSchema } from './user.schema';

@ClientControllers('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Get('detail/:userId')
  @Public()
  @ApiOperation({
    summary: 'Get detail user',
  })
  @ApiResponse(DetailUserResponseSchema)
  async detailUser(@Param('userId', ValidateMongoIdPipe) userId: string) {
    return this.userService.detailUser(userId);
  }

  @Get('get-list')
  @Public()
  @ApiOperation({
    summary: 'Get list user',
  })
  @ApiResponse(DetailUserResponseSchema)
  async getListUser() {
    return this.userService.getListUser();
  }
}
