import { HttpStatus } from '@nestjs/common';
import { ApiResponseOptions } from '@nestjs/swagger';

export const DetailUserResponseSchema: ApiResponseOptions = {
  status: HttpStatus.OK,
  description: 'Response login example',
  schema: {
    example: {
      data: {
        _id: '65a733cb824c7e8c4482fd30',
        name: 'vanhien',
        email: 'tvhien2000az@gmail.com',
        createdAt: '2024-01-17T01:56:27.163Z',
        updatedAt: '2024-01-17T01:56:27.271Z',
        __v: 0,
      },
    },
  },
};
