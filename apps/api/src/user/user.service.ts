import { User } from '@app/database-mongodb/schemas/user';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Model } from 'mongoose';

@Injectable()
export class UserService {
  constructor(@InjectModel(User.name) private userModel: Model<User>) {}
  async detailUser(userId: string) {
    return this.userModel.findById(new mongoose.Types.ObjectId(userId));
  }

  async getListUser() {
    return this.userModel.find();
  }
}
