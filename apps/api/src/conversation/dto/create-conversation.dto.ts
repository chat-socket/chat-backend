import { ArrayMinSize, IsArray, IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';

export class CreateConversationDto {
  /**
   * user id
   * @example  [1]
   */
  @IsNotEmpty()
  @IsArray()
  @ArrayMinSize(1)
  @IsString({ each: true })
  userIds: string[];

  /**
   * your name
   * @example 0Azhihahaxxxx
   */

  @IsNotEmpty()
  @IsString()
  @MinLength(3)
  @MaxLength(30)
  name: string;
}
