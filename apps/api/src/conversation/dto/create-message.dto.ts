import { IsNotEmpty, IsString, MaxLength, MinLength } from 'class-validator';

export class CreateMessageDto {
  /**
   * conversation id
   * @example  11111111111111111111111111111111
   */
  @IsNotEmpty()
  @IsString()
  conversationId: string;

  /**
   * text
   * @example 0Azhihahaxxxx
   */

  @IsNotEmpty()
  @IsString()
  @MinLength(1)
  @MaxLength(5000)
  text: string;
}
