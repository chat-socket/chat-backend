import { Injectable } from '@nestjs/common';
import { CreateConversationDto } from './dto/create-conversation.dto';
import { InjectModel } from '@nestjs/mongoose';
import { Conversation } from '@app/database-mongodb/schemas/conversation';
import { Model } from 'mongoose';
import { User } from '@app/database-mongodb/schemas/user';
import { Exception } from '@app/core/exception';
import { ErrorCustom } from 'libs/constants/enum';
import { CreateMessageDto } from './dto/create-message.dto';
import { Message } from '@app/database-mongodb/schemas/message';

@Injectable()
export class ConversationService {
  constructor(
    @InjectModel(Conversation.name) private conversationModel: Model<Conversation>,
    @InjectModel(User.name) private userModel: Model<User>,
    @InjectModel(Message.name) private messageModel: Model<Message>,
  ) {}

  async createConversation(userId: string, body: CreateConversationDto) {
    const targetId = body.userIds[0];
    const conversation = await this.conversationModel.findOne({
      userIds: { $all: [userId, targetId] },
    });

    if (conversation) return conversation;

    const user = await this.userModel.findOne({ _id: targetId });

    if (!user) {
      throw new Exception(ErrorCustom.User_Not_Found, 'Không tìm thấy user');
    }

    const newConversation = new this.conversationModel({
      userIds: [userId, targetId],
      name: body.name,
    });

    const response = await newConversation.save();
    return response;
  }

  async getListConversation(userId: string) {
    return this.conversationModel.find({ userIds: { $in: [userId] } });
  }

  async createMessage(userId: string, body: CreateMessageDto) {
    const conversation = await this.conversationModel.findById(body.conversationId);

    if (!conversation) {
      throw new Exception(ErrorCustom.Conversation_Not_Found, 'Không tìm thấy conversation');
    }

    const message = new this.messageModel({ ...body, userId });
    return message.save();
  }

  async getListMessage(userId: string, conversationId: string) {
    const conversation = await this.conversationModel.findOne({ _id: conversationId, userIds: { $in: [userId] } });

    if (!conversation) {
      throw new Exception(
        ErrorCustom.Conversation_Not_Found,
        'Không tìm thấy conversation hoặc thằng này không trong cuộc trò truyện này',
      );
    }

    return this.messageModel.find({ conversationId });
  }
}
