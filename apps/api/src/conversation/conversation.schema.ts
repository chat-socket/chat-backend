import { HttpStatus } from '@nestjs/common';
import { ApiResponseOptions } from '@nestjs/swagger';

export const CreateConversationSchema: ApiResponseOptions = {
  status: HttpStatus.OK,
  description: 'Response login example',
  schema: {
    example: {
      data: {
        _id: '65b8b782b8eb702a1486cf0c',
        name: 'hell1',
        userIds: ['65b3175e90114fd334521ec8', '65b3188722d6c48a8282d9cd'],
        createdAt: '2024-01-30T08:46:58.719Z',
        updatedAt: '2024-01-30T08:46:58.719Z',
        __v: 0,
      },
    },
  },
};

export const GetListConversationSchema: ApiResponseOptions = {
  status: HttpStatus.OK,
  description: 'Response login example',
  schema: {
    example: {
      data: [
        {
          _id: '65b8b782b8eb702a1486cf0c',
          name: 'hell1',
          userIds: ['65b3175e90114fd334521ec8', '65b3188722d6c48a8282d9cd'],
          createdAt: '2024-01-30T08:46:58.719Z',
          updatedAt: '2024-01-30T08:46:58.719Z',
          __v: 0,
        },
        {
          _id: '65b8bd187644d6edf514fe62',
          name: 'hell2',
          userIds: ['65b3175e90114fd334521ec8', '65b318d829f14f50fbb164ee'],
          createdAt: '2024-01-30T09:10:48.455Z',
          updatedAt: '2024-01-30T09:10:48.455Z',
          __v: 0,
        },
      ],
    },
  },
};

export const CreateMessageSchema: ApiResponseOptions = {
  status: HttpStatus.OK,
  description: 'Response login example',
  schema: {
    example: {
      data: {
        conversationId: '65b8b782b8eb702a1486cf0c',
        userId: '65b3175e90114fd334521ec8',
        text: 'do ngu do an hai',
        _id: '65b9f79f67314060399941f9',
        createdAt: '2024-01-31T07:32:47.519Z',
        updatedAt: '2024-01-31T07:32:47.519Z',
        __v: 0,
      },
    },
  },
};

export const ListMessageSchema: ApiResponseOptions = {
  status: HttpStatus.OK,
  description: 'Response login example',
  schema: {
    example: {
      data: [
        {
          _id: '65b9f79f67314060399941f9',
          conversationId: '65b8b782b8eb702a1486cf0c',
          userId: '65b3175e90114fd334521ec8',
          text: 'do ngu do an hai',
          createdAt: '2024-01-31T07:32:47.519Z',
          updatedAt: '2024-01-31T07:32:47.519Z',
          __v: 0,
        },
        {
          _id: '65b9f9259130dfb0c4ee67e6',
          conversationId: '65b8b782b8eb702a1486cf0c',
          userId: '65b3175e90114fd334521ec8',
          text: 'do ngu do an hai 2',
          createdAt: '2024-01-31T07:39:17.991Z',
          updatedAt: '2024-01-31T07:39:17.991Z',
          __v: 0,
        },
      ],
    },
  },
};
