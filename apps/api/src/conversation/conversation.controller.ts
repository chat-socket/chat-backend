import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ConversationService } from './conversation.service';
import { ClientControllers } from '@app/core/decorator/controller-customer.decorator';
import { UserData } from '@app/core/decorator/user.decorator';
import { Express } from 'express';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { CreateConversationDto } from './dto/create-conversation.dto';
import {
  CreateConversationSchema,
  CreateMessageSchema,
  GetListConversationSchema,
  ListMessageSchema,
} from './conversation.schema';
import { CreateMessageDto } from './dto/create-message.dto';
import { ValidateMongoIdPipe } from '@app/core/pipes/validation.pipe';

@ClientControllers('conversation')
export class ConversationController {
  constructor(private readonly conversationService: ConversationService) {}

  @Post('create')
  @ApiOperation({
    summary: 'create conversation',
  })
  @ApiResponse(CreateConversationSchema)
  createConversation(@UserData() user: Express.User, @Body() body: CreateConversationDto) {
    return this.conversationService.createConversation(user._id, body);
  }

  @Get('list')
  @ApiOperation({
    summary: 'get list conversation',
  })
  @ApiResponse(GetListConversationSchema)
  getListConversation(@UserData() user: Express.User) {
    return this.conversationService.getListConversation(user._id);
  }

  @Post('create-message')
  @ApiOperation({
    summary: 'create message',
  })
  @ApiResponse(CreateMessageSchema)
  createMessage(@UserData() user: Express.User, @Body() body: CreateMessageDto) {
    return this.conversationService.createMessage(user._id, body);
  }

  @Get('list-message/:conversationId')
  @ApiOperation({
    summary: 'get list message',
  })
  @ApiResponse(ListMessageSchema)
  getListMessage(@UserData() user: Express.User, @Param('conversationId', ValidateMongoIdPipe) conversationId: string) {
    return this.conversationService.getListMessage(user._id, conversationId);
  }
}
