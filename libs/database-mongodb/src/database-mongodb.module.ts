import { MiddlewareConsumer, Module } from '@nestjs/common';
import { DatabaseMongodbService } from './database-mongodb.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { IConfig } from 'libs/constants/configuration-env';
import { set } from 'mongoose';
import { Environment } from 'libs/constants/enum';

@Module({
  imports: [
    MongooseModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService<IConfig, true>) => {
        const uri = configService.get('database').mongodbUri;
        if (!uri) {
          throw new Error('Missing configuration "database.mongodbUri"');
        }

        return { uri };
      },
      inject: [ConfigService],
    }),
  ],

  providers: [DatabaseMongodbService],
  exports: [DatabaseMongodbService],
})
export class DatabaseMongodbModule {
  constructor(private configService: ConfigService<IConfig, true>) {}

  configure(consumer: MiddlewareConsumer) {
    const nodeEnv = this.configService.get<Environment>('nodeEnv');

    if (![Environment.Production].includes(nodeEnv)) {
      // enable debug Mongoose
      set('debug', true);
    }
  }
}
