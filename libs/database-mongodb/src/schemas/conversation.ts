import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, set } from 'mongoose';

export type ConversationDocument = Document<Conversation>;

@Schema({ timestamps: true, collection: 'conversations' })
export class Conversation {
  @Prop({ required: true, type: String, minlength: 3, maxlength: 30 })
  name: string;

  @Prop({ required: true, type: Array, minlength: 2 })
  userIds: string[];

  @Prop({ required: false, type: String })
  avatar?: string;
}

export const ConversationSchema = SchemaFactory.createForClass(Conversation);
