import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document, set } from 'mongoose';

export type UserDocument = Document<User>;

@Schema({ timestamps: true, collection: 'users' })
export class User {
  @Prop({ required: true, type: String, minlength: 3, maxlength: 30 })
  name: string;

  @Prop({ required: true, type: String, minlength: 3, maxlength: 30, unique: true, index: true })
  email: string;

  @Prop({ required: true, type: String, minlength: 6, maxlength: 1024, select: false })
  password: string;

  @Prop({ required: false, type: String, minlength: 6, maxlength: 1024, select: false })
  refreshToken: string;

  @Prop({ required: false, type: String })
  avatar?: string;
}

export const UserSchema = SchemaFactory.createForClass(User);
