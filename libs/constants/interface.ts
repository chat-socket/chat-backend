import { UserType } from './enum';

export interface IToken {
  token: string;
  refreshToken: string;
  _id: string;
  name: string;
  email: string;
}
export interface IPayloadToken {
  _id: number;
  userType: UserType;
  timeStamp: number;
}
export interface IDataTokenForgotPassword {
  email: string;
  timeStamp: number;
  userType: number;
  id: number;
  iat: number;
  exp: number;
}
